package;

import sys.FileSystem;

import tink.http.Fetch;
import sys.io.File;
import hl.UI;
import haxe.Json;

/**
 * ...
 * @author kevansevans
 */
class Main 
{
	static var arguments = Sys.args;
	static function main() 
	{
		if (!FileSystem.exists("./linerider.hl")) {
			Sys.println("Failure to detect bytecode");
			download_bytecode(); //First time run
			boot();
		}
		else start_updates();	
	}
	static function start_updates() 
	{
		if (!FileSystem.exists("info_new.txt")) {
			download_info();
		}
		else check_updates();
	}
	static function download_bytecode() {
		Sys.println("Downloading from https://gitlab.com/kevansevans/hxlr");
		Fetch.fetch("https://gitlab.com/kevansevans/hxlr/raw/master/linerider/bin/HXLR.hl?inline=false").all()
		.handle(function(o) switch(o) {
			case Success(res):
				Sys.println("Sucessfully downloaded linerider.hl");
				var bytes = res.body.toBytes();
				var out = File.write("linerider.hl");
				out.writeBytes(bytes, 0, bytes.length);
				out.close();
			case Failure(e):
				trace("Failed to download linerider.hl");
		});
	}
	static function download_info() {
		Fetch.fetch("https://gitlab.com/kevansevans/hxlr/raw/master/info.txt?inline=false").all()
		.handle(function(o) switch(o) {
			case Success(res):
				var bytes = res.body.toBytes();
				var name:String = "info.txt";
				if (FileSystem.exists("info.txt")) name = "info_new.txt";
				var out = File.write(name);
				out.writeBytes(bytes, 0, bytes.length);
				out.close();
				Sys.sleep(1);
				check_updates();
			case Failure(e):
				trace("Failed to download info.txt. Please manually download this file.");
		});
	}
	static function check_updates() 
	{
		if (!FileSystem.exists("./info_new.txt")) {
			boot();
			return;
		}
		var old_info = Json.parse(File.getContent("./info.txt"));
		var new_info = Json.parse(File.getContent("./info_new.txt"));
		
		var ver:String = "" + Std.string(new_info.version) + "." + Std.string(new_info.patch) + "." + Std.string(new_info.hotfix);
		if (old_info.beta == true) {
			ask_to_update(ver, true);
			return;
		}
		else {
			if (new_info.hotfix > old_info.hotfix) {
				if (old_info.get_hot == 0) {
					ask_to_update(ver, false);
					return;
				} else if (old_info.get_hot == 1) {
					download_bytecode();
					return;
				} else if (old_info.get_hot == -1) {}
			} 
			else if (new_info.patch > old_info.patch) {
				if (old_info.get_patch == 0) {
					ask_to_update(ver, false);
					return;
				} else if (old_info.get_patch == 1) {
					download_bytecode();
					return;
				} else if (old_info.get_patch == -1) {}
			} 
			else if (new_info.version > old_info.version) {
				if (old_info.get_version == 0) {
					ask_to_update(ver, false);
					return;
				} else if (old_info.get_version == 1) {
					download_bytecode();
					return;
				} else if (old_info.get_version == -1) {}
			}
		}
		boot();
	}
	static function ask_to_update(_ver:String, _beta:Bool) {
		if (!_beta) Sys.println('A new version has been detected, would you like to update to $_ver? Y/N?');
		else Sys.println('The latest version is $_ver, but your settings indicate you\'d like to download the latest push. Continue with download? Y/N?');
		var p1:String = Sys.stdin().readLine();
		switch (p1) {
			case "y" | "Y" | "Yes":
				download_bytecode();
				boot();
			case "n" | "N" | "No":
				boot();
		}
	}
	static function boot() 
	{
		if (FileSystem.exists("info_new.txt")) {
			var old_info = Json.parse(File.getContent("./info.txt"));
			var new_info = Json.parse(File.getContent("./info_new.txt"));
			
			old_info.version = new_info.version;
			old_info.patch = new_info.patch;
			old_info.hotfix = new_info.hotfix;
			
			var output = Json.stringify(old_info);
			FileSystem.deleteFile("info_new.txt");
			var out = File.write("info.txt");
			out.writeString(output);
			out.close();
		}
		UI.closeConsole();
		if (FileSystem.exists("linerider.exe")) Sys.command("linerider.exe", ["./linerider.hl"]);
		else Sys.command("hl.exe", ["./linerider.hl"]);
	}
	
}