package;

import hl.UI;
import hxd.App;
import sys.FileSystem;

import com.geom.Point;
import lr.engine.lines.LineBase;
import lr.scene.Canvas;

/**
 * ...
 * @author kevansevans
 */
class Main extends App
{
	
	var canvas:Canvas;
	var dummy_line:LineBase;
	var line_count:Int;
	var line_array:Array<LineBase> = new Array();
	static function main() 
	{
		new Main();
	}
	override function init() 
	{
		UI.closeConsole();
		engine.backgroundColor = 0xCCCCCC;
		
		canvas = new Canvas();
		
		s2d.addChild(canvas);
		
		dummy_line = new LineBase(new Point(0, 0), new Point(500, 500), 0);
		canvas.addChild(dummy_line);
	}
	override function update(dt:Float) 
	{
		
	}
	function randomRange(minNum:Int, maxNum:Int):Int  
	{ 
		return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum); 
	}
}