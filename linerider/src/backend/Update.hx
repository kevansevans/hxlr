package backend;

import sys.FileSystem;

/**
 * ...
 * @author kevansevans
 */
enum abstract UpdateCheck(Int) from Int {
	var never:Int;
	var ask:Int;
	var always:Int;
}
class Update 
{
	static inline var version:Int = 0;
	static inline var update:Int = 0;
	static inline var hotfix:Int = 0;
	
	var u_version:Int = UpdateCheck.ask;
	var u_update:Int = UpdateCheck.ask;
	var u_hotfix:Int = UpdateCheck.always;
	public function new() 
	{
		
	}
}