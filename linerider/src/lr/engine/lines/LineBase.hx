package lr.engine.lines;
import com.geom.Point;

import h2d.Graphics;


/**
 * ...
 * @author kevansevans
 */
enum abstract LineType(Null<Int>) from Null<Int> {
	var NULL:Null<Int> = null;
	var FLOOR:Null<Int> = 0;
	var ACCEL:Null<Int>;
	var SCENE:Null<Int>;
}
enum abstract LineHex(Int) from Int {
	var UNDEFINED:Int = 0xCC00CC;
	var NULL:Int = 0xFF0000;
	var FLOOR:Int = 0x0066FF;
	var ACCEL:Int = 0xCC0000;
	var SCENE:Int = 0x00CC00;
}
class LineBase extends Graphics
{
	public var a:Point;
	public var b:Point;
	public var type:Null<Int>;
	public function new(_a:Point, _b:Point, ?_type:Int) 
	{
		super();
		
		a = _a;
		b = _b;
		type = _type;
		
		this.render();
	}
	function render() {
		switch (type) {
			case (LineType.NULL) :
				lineStyle(2, LineHex.NULL, 1);
			case (LineType.FLOOR) :
				lineStyle(2, LineHex.FLOOR, 1);
			case (LineType.ACCEL) :
				lineStyle(2, LineHex.ACCEL, 1);
			case (LineType.SCENE) :
				lineStyle(2, LineHex.SCENE, 1);
			default :
				lineStyle(2, LineHex.UNDEFINED, 1);
		}
		moveTo(a.x, a.y);
		lineTo(b.x, b.y);
		trace("blep");
	}
}