package lr.engine.bodies.group;
import lr.engine.bodies.anchors.CPoint;

/**
 * ...
 * @author kevansevans
 */
class GroupBase 
{
	public var contact_points:Array<CPoint>;
	public var scarf_points:Array<CPoint>;
	public var body_constraints:Array<CPoint>;
	public function new() {
		
	}
	public function iterate() {
		
	}
	public function constrain() {
		
	}
	public function validate() {
		
	}
	public function render() {
		
	}
}