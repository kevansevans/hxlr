package com.geom;

/**
 * ...
 * @author kevansevans
 */
class Point 
{
	public var x:Float;
	public var y:Float;
	public function new(_x:Null<Float> = 0, _y:Null<Float> = 0) 
	{
		x = _x;
		y = _y;
	}
}